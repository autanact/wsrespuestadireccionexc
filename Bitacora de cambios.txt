#Bitacora de cambios servicio WSRespuestaDireccionExc
#Freddy Molina
#Fecha creación:28/11/2015
#Descripión general: Servicio que se encarga del envío de dirección excepcionada a GDE
#Últimas modificaciónes
	28/11/2015 FMS Se crea WSDL e implementación
	03/03/2016 FMS Documentación
#Versión actual
	22/02/2016 v2.0