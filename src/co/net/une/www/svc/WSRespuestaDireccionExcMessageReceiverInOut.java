
/**
 * WSRespuestaDireccionExcMessageReceiverInOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:35 LKT)
 */
        package co.net.une.www.svc;

        /**
        *  WSRespuestaDireccionExcMessageReceiverInOut message receiver
        */

        public class WSRespuestaDireccionExcMessageReceiverInOut extends org.apache.axis2.receivers.AbstractInOutMessageReceiver{


        public void invokeBusinessLogic(org.apache.axis2.context.MessageContext msgContext, org.apache.axis2.context.MessageContext newMsgContext)
        throws org.apache.axis2.AxisFault{

        try {

        // get the implementation class for the Web Service
        Object obj = getTheImplementationObject(msgContext);

        WSRespuestaDireccionExcSkeleton skel = (WSRespuestaDireccionExcSkeleton)obj;
        //Out Envelop
        org.apache.axiom.soap.SOAPEnvelope envelope = null;
        //Find the axisOperation that has been set by the Dispatch phase.
        org.apache.axis2.description.AxisOperation op = msgContext.getOperationContext().getAxisOperation();
        if (op == null) {
        throw new org.apache.axis2.AxisFault("Operation is not located, if this is doclit style the SOAP-ACTION should specified via the SOAP Action to use the RawXMLProvider");
        }

        java.lang.String methodName;
        if((op.getName() != null) && ((methodName = org.apache.axis2.util.JavaUtils.xmlNameToJava(op.getName().getLocalPart())) != null)){

        

            if("respuestaDireccionExc".equals(methodName)){
                
                co.net.une.www.svc.WSRespuestaDireccionExcRS wSRespuestaDireccionExcRS23 = null;
	                        co.net.une.www.svc.WSRespuestaDireccionExcRQ wrappedParam =
                                                             (co.net.une.www.svc.WSRespuestaDireccionExcRQ)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    co.net.une.www.svc.WSRespuestaDireccionExcRQ.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               wSRespuestaDireccionExcRS23 =
                                                   
                                                   
                                                           wrapWSRespuestaDireccionExcRSGisRespuestaProceso(
                                                       
                                                        

                                                        
                                                       skel.respuestaDireccionExc(
                                                            
                                                                getCodigoSolicitud(wrappedParam)
                                                            ,
                                                                getDireccionNormalizada(wrappedParam)
                                                            ,
                                                                getEstadoGeoreferenciacion(wrappedParam)
                                                            ,
                                                                getCodigoDaneManzana(wrappedParam)
                                                            ,
                                                                getCodigoComuna(wrappedParam)
                                                            ,
                                                                getLatitud(wrappedParam)
                                                            ,
                                                                getLongitud(wrappedParam)
                                                            ,
                                                                getNombreBarrio(wrappedParam)
                                                            ,
                                                                getCodigoBarrio(wrappedParam)
                                                            ,
                                                                getPlaca(wrappedParam)
                                                            ,
                                                                getAgregado(wrappedParam)
                                                            ,
                                                                getRemanente(wrappedParam)
                                                            ,
                                                                getEstrato(wrappedParam)
                                                            ,
                                                                getRural(wrappedParam)
                                                            ,
                                                                getCodigoDireccionProveevor(wrappedParam)
                                                            ,
                                                                getCodigoPais(wrappedParam)
                                                            ,
                                                                getCodigoDepartamento(wrappedParam)
                                                            ,
                                                                getCodigoMunicipio(wrappedParam)
                                                            ,
                                                                getCodigoPredio(wrappedParam)
                                                            ,
                                                                getEstadoExcepcion(wrappedParam)
                                                            ,
                                                                getComentarioExcepcion(wrappedParam)
                                                            ,
                                                                getFechaRespuestaProveedor(wrappedParam)
                                                            )
                                                    
                                                         )
                                                     ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), wSRespuestaDireccionExcRS23, false);
                                    
            } else {
              throw new java.lang.RuntimeException("method not found");
            }
        

        newMsgContext.setEnvelope(envelope);
        }
        }
        catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
        }
        
        //
            private  org.apache.axiom.om.OMElement  toOM(co.net.une.www.svc.WSRespuestaDireccionExcRQ param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(co.net.une.www.svc.WSRespuestaDireccionExcRQ.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(co.net.une.www.svc.WSRespuestaDireccionExcRS param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(co.net.une.www.svc.WSRespuestaDireccionExcRS.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, co.net.une.www.svc.WSRespuestaDireccionExcRS param, boolean optimizeContent)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(co.net.une.www.svc.WSRespuestaDireccionExcRS.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    

                        private co.net.une.www.svc.BoundedString30 getCodigoSolicitud(
                        co.net.une.www.svc.WSRespuestaDireccionExcRQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDireccionExcRQ().getCodigoSolicitud();
                            
                        }
                     

                        private co.net.une.www.svc.BoundedString100 getDireccionNormalizada(
                        co.net.une.www.svc.WSRespuestaDireccionExcRQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDireccionExcRQ().getDireccionNormalizada();
                            
                        }
                     

                        private co.net.une.www.svc.BoundedString1 getEstadoGeoreferenciacion(
                        co.net.une.www.svc.WSRespuestaDireccionExcRQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDireccionExcRQ().getEstadoGeoreferenciacion();
                            
                        }
                     

                        private co.net.une.www.svc.BoundedString20 getCodigoDaneManzana(
                        co.net.une.www.svc.WSRespuestaDireccionExcRQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDireccionExcRQ().getCodigoDaneManzana();
                            
                        }
                     

                        private co.net.une.www.svc.BoundedString20 getCodigoComuna(
                        co.net.une.www.svc.WSRespuestaDireccionExcRQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDireccionExcRQ().getCodigoComuna();
                            
                        }
                     

                        private float getLatitud(
                        co.net.une.www.svc.WSRespuestaDireccionExcRQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDireccionExcRQ().getLatitud();
                            
                        }
                     

                        private float getLongitud(
                        co.net.une.www.svc.WSRespuestaDireccionExcRQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDireccionExcRQ().getLongitud();
                            
                        }
                     

                        private co.net.une.www.svc.BoundedString100 getNombreBarrio(
                        co.net.une.www.svc.WSRespuestaDireccionExcRQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDireccionExcRQ().getNombreBarrio();
                            
                        }
                     

                        private co.net.une.www.svc.BoundedString20 getCodigoBarrio(
                        co.net.une.www.svc.WSRespuestaDireccionExcRQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDireccionExcRQ().getCodigoBarrio();
                            
                        }
                     

                        private co.net.une.www.svc.BoundedString20 getPlaca(
                        co.net.une.www.svc.WSRespuestaDireccionExcRQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDireccionExcRQ().getPlaca();
                            
                        }
                     

                        private co.net.une.www.svc.BoundedString100 getAgregado(
                        co.net.une.www.svc.WSRespuestaDireccionExcRQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDireccionExcRQ().getAgregado();
                            
                        }
                     

                        private co.net.une.www.svc.BoundedString100 getRemanente(
                        co.net.une.www.svc.WSRespuestaDireccionExcRQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDireccionExcRQ().getRemanente();
                            
                        }
                     

                        private java.math.BigInteger getEstrato(
                        co.net.une.www.svc.WSRespuestaDireccionExcRQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDireccionExcRQ().getEstrato();
                            
                        }
                     

                        private co.net.une.www.svc.BoundedString2 getRural(
                        co.net.une.www.svc.WSRespuestaDireccionExcRQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDireccionExcRQ().getRural();
                            
                        }
                     

                        private co.net.une.www.svc.BoundedString100 getCodigoDireccionProveevor(
                        co.net.une.www.svc.WSRespuestaDireccionExcRQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDireccionExcRQ().getCodigoDireccionProveevor();
                            
                        }
                     

                        private co.net.une.www.svc.BoundedString20 getCodigoPais(
                        co.net.une.www.svc.WSRespuestaDireccionExcRQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDireccionExcRQ().getCodigoPais();
                            
                        }
                     

                        private co.net.une.www.svc.BoundedString20 getCodigoDepartamento(
                        co.net.une.www.svc.WSRespuestaDireccionExcRQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDireccionExcRQ().getCodigoDepartamento();
                            
                        }
                     

                        private co.net.une.www.svc.BoundedString20 getCodigoMunicipio(
                        co.net.une.www.svc.WSRespuestaDireccionExcRQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDireccionExcRQ().getCodigoMunicipio();
                            
                        }
                     

                        private co.net.une.www.svc.BoundedString20 getCodigoPredio(
                        co.net.une.www.svc.WSRespuestaDireccionExcRQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDireccionExcRQ().getCodigoPredio();
                            
                        }
                     

                        private co.net.une.www.svc.BoundedString20 getEstadoExcepcion(
                        co.net.une.www.svc.WSRespuestaDireccionExcRQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDireccionExcRQ().getEstadoExcepcion();
                            
                        }
                     

                        private co.net.une.www.svc.BoundedString400 getComentarioExcepcion(
                        co.net.une.www.svc.WSRespuestaDireccionExcRQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDireccionExcRQ().getComentarioExcepcion();
                            
                        }
                     

                        private co.net.une.www.svc.BoundedString19 getFechaRespuestaProveedor(
                        co.net.une.www.svc.WSRespuestaDireccionExcRQ wrappedType){
                        
                                return wrappedType.getWSRespuestaDireccionExcRQ().getFechaRespuestaProveedor();
                            
                        }
                     
                        private co.net.une.www.svc.WSRespuestaDireccionExcRQType getrespuestaDireccionExc(
                        co.net.une.www.svc.WSRespuestaDireccionExcRQ wrappedType){
                            return wrappedType.getWSRespuestaDireccionExcRQ();
                        }
                        
                        
                    

                        
                        private co.net.une.www.svc.WSRespuestaDireccionExcRS wrapWSRespuestaDireccionExcRSGisRespuestaProceso(
                        co.net.une.www.svc.GisRespuestaGeneralType param){
                        co.net.une.www.svc.WSRespuestaDireccionExcRS wrappedElement = new co.net.une.www.svc.WSRespuestaDireccionExcRS();
                        co.net.une.www.svc.WSRespuestaDireccionExcRSType innerType = new co.net.une.www.svc.WSRespuestaDireccionExcRSType();
                                innerType.setGisRespuestaProceso(param);
                                wrappedElement.setWSRespuestaDireccionExcRS(innerType);
                            
                            return wrappedElement;
                        }
                     
                         private co.net.une.www.svc.WSRespuestaDireccionExcRS wraprespuestaDireccionExc(
                            co.net.une.www.svc.WSRespuestaDireccionExcRSType innerType){
                                co.net.une.www.svc.WSRespuestaDireccionExcRS wrappedElement = new co.net.une.www.svc.WSRespuestaDireccionExcRS();
                                wrappedElement.setWSRespuestaDireccionExcRS(innerType);
                                return wrappedElement;
                         }
                    


        /**
        *  get the default envelope
        */
        private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory){
        return factory.getDefaultEnvelope();
        }


        private  java.lang.Object fromOM(
        org.apache.axiom.om.OMElement param,
        java.lang.Class type,
        java.util.Map extraNamespaces) throws org.apache.axis2.AxisFault{

        try {
        
                if (co.net.une.www.svc.WSRespuestaDireccionExcRQ.class.equals(type)){
                
                           return co.net.une.www.svc.WSRespuestaDireccionExcRQ.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (co.net.une.www.svc.WSRespuestaDireccionExcRS.class.equals(type)){
                
                           return co.net.une.www.svc.WSRespuestaDireccionExcRS.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
        } catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
           return null;
        }



    

        /**
        *  A utility method that copies the namepaces from the SOAPEnvelope
        */
        private java.util.Map getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env){
        java.util.Map returnMap = new java.util.HashMap();
        java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
        while (namespaceIterator.hasNext()) {
        org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
        returnMap.put(ns.getPrefix(),ns.getNamespaceURI());
        }
        return returnMap;
        }

        private org.apache.axis2.AxisFault createAxisFault(java.lang.Exception e) {
        org.apache.axis2.AxisFault f;
        Throwable cause = e.getCause();
        if (cause != null) {
            f = new org.apache.axis2.AxisFault(e.getMessage(), cause);
        } else {
            f = new org.apache.axis2.AxisFault(e.getMessage());
        }

        return f;
    }

        }//end of class
    