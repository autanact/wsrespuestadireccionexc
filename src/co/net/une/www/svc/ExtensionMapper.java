
/**
 * ExtensionMapper.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:41 LKT)
 */

            package co.net.une.www.svc;
            /**
            *  ExtensionMapper class
            */
        
        public  class ExtensionMapper{

          public static java.lang.Object getTypeObject(java.lang.String namespaceURI,
                                                       java.lang.String typeName,
                                                       javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{

              
                  if (
                  "http://www.une.net.co/svc".equals(namespaceURI) &&
                  "boundedString400".equals(typeName)){
                   
                            return  co.net.une.www.svc.BoundedString400.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/svc".equals(namespaceURI) &&
                  "boundedString100".equals(typeName)){
                   
                            return  co.net.une.www.svc.BoundedString100.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/svc".equals(namespaceURI) &&
                  "boundedString19".equals(typeName)){
                   
                            return  co.net.une.www.svc.BoundedString19.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/svc".equals(namespaceURI) &&
                  "boundedString2".equals(typeName)){
                   
                            return  co.net.une.www.svc.BoundedString2.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/svc".equals(namespaceURI) &&
                  "WSRespuestaDireccionExc-RQ-Type".equals(typeName)){
                   
                            return  co.net.une.www.svc.WSRespuestaDireccionExcRQType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/svc".equals(namespaceURI) &&
                  "boundedString1".equals(typeName)){
                   
                            return  co.net.une.www.svc.BoundedString1.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/svc".equals(namespaceURI) &&
                  "GisRespuestaGeneralType".equals(typeName)){
                   
                            return  co.net.une.www.svc.GisRespuestaGeneralType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/svc".equals(namespaceURI) &&
                  "WSRespuestaDireccionExc-RS-Type".equals(typeName)){
                   
                            return  co.net.une.www.svc.WSRespuestaDireccionExcRSType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/svc".equals(namespaceURI) &&
                  "boundedString30".equals(typeName)){
                   
                            return  co.net.une.www.svc.BoundedString30.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/svc".equals(namespaceURI) &&
                  "boundedString20".equals(typeName)){
                   
                            return  co.net.une.www.svc.BoundedString20.Factory.parse(reader);
                        

                  }

              
             throw new org.apache.axis2.databinding.ADBException("Unsupported type " + namespaceURI + " " + typeName);
          }

        }
    