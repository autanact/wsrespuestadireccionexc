
/**
 * WSRespuestaDECallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:35 LKT)
 */

    package co.net.une.www.svc;

    /**
     *  WSRespuestaDECallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class WSRespuestaDECallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public WSRespuestaDECallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public WSRespuestaDECallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for respuestaDE method
            * override this method for handling normal response from respuestaDE operation
            */
           public void receiveResultrespuestaDE(
                    co.net.une.www.svc.WSRespuestaDEStub.WSRespuestaDERS result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from respuestaDE operation
           */
            public void receiveErrorrespuestaDE(java.lang.Exception e) {
            }
                


    }
    