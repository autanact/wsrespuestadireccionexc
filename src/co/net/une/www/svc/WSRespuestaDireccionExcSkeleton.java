
/**
 * WSRespuestaDireccionExcSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.3  Built on : Aug 10, 2007 (04:45:47 LKT)
 * with extensions for GE Smallworld GeoSpatial Server
 */
    package co.net.une.www.svc;
    import java.util.Map;
    import java.util.HashMap;    
    
    import org.apache.axis2.engine.AxisError;
    
    import com.gesmallworld.gss.lib.exception.GSSException;
    import com.gesmallworld.gss.webservice.WebServiceRequest;
    /**
     *  WSRespuestaDireccionExcSkeleton java skeleton for the axisService
     */
    public class WSRespuestaDireccionExcSkeleton extends WebServiceRequest
        {
        
	
	private static final String serviceName = "ejb/WSRespuestaDireccionExcLocal";
	
     
         
        /**
         * Auto generated method signature
         
         
                                     * @param codigoSolicitud
                                     * @param direccionNormalizada
                                     * @param estadoGeoreferenciacion
                                     * @param codigoDaneManzana
                                     * @param codigoComuna
                                     * @param latitud
                                     * @param longitud
                                     * @param nombreBarrio
                                     * @param codigoBarrio
                                     * @param placa
                                     * @param agregado
                                     * @param remanente
                                     * @param estrato
                                     * @param rural
                                     * @param codigoDireccionProveevor
                                     * @param codigoPais
                                     * @param codigoDepartamento
                                     * @param codigoMunicipio
                                     * @param codigoPredio
                                     * @param estadoExcepcion
                                     * @param comentarioExcepcion
                                     * @param fechaRespuestaProveedor
         */
        

                 public co.net.une.www.svc.GisRespuestaGeneralType respuestaDireccionExc
                  (
                  co.net.une.www.svc.BoundedString30 codigoSolicitud,co.net.une.www.svc.BoundedString100 direccionNormalizada,co.net.une.www.svc.BoundedString1 estadoGeoreferenciacion,co.net.une.www.svc.BoundedString20 codigoDaneManzana,co.net.une.www.svc.BoundedString20 codigoComuna,float latitud,float longitud,co.net.une.www.svc.BoundedString100 nombreBarrio,co.net.une.www.svc.BoundedString20 codigoBarrio,co.net.une.www.svc.BoundedString20 placa,co.net.une.www.svc.BoundedString100 agregado,co.net.une.www.svc.BoundedString100 remanente,java.math.BigInteger estrato,co.net.une.www.svc.BoundedString2 rural,co.net.une.www.svc.BoundedString100 codigoDireccionProveevor,co.net.une.www.svc.BoundedString20 codigoPais,co.net.une.www.svc.BoundedString20 codigoDepartamento,co.net.une.www.svc.BoundedString20 codigoMunicipio,co.net.une.www.svc.BoundedString20 codigoPredio,co.net.une.www.svc.BoundedString20 estadoExcepcion,co.net.une.www.svc.BoundedString400 comentarioExcepcion,co.net.une.www.svc.BoundedString19 fechaRespuestaProveedor
                  )
            {
                //GSS generated code
		Map<String,Object> params = new HashMap<String,Object>();
                  params.put("codigoSolicitud",codigoSolicitud);params.put("direccionNormalizada",direccionNormalizada);params.put("estadoGeoreferenciacion",estadoGeoreferenciacion);params.put("codigoDaneManzana",codigoDaneManzana);params.put("codigoComuna",codigoComuna);params.put("latitud",latitud);params.put("longitud",longitud);params.put("nombreBarrio",nombreBarrio);params.put("codigoBarrio",codigoBarrio);params.put("placa",placa);params.put("agregado",agregado);params.put("remanente",remanente);params.put("estrato",estrato);params.put("rural",rural);params.put("codigoDireccionProveevor",codigoDireccionProveevor);params.put("codigoPais",codigoPais);params.put("codigoDepartamento",codigoDepartamento);params.put("codigoMunicipio",codigoMunicipio);params.put("codigoPredio",codigoPredio);params.put("estadoExcepcion",estadoExcepcion);params.put("comentarioExcepcion",comentarioExcepcion);params.put("fechaRespuestaProveedor",fechaRespuestaProveedor);
		try{
		
			return (co.net.une.www.svc.GisRespuestaGeneralType)
			this.makeStructuredRequest(serviceName, "respuestaDireccionExc", params);
		}catch(GSSException e){
                    // Modify if specific faults are required
                    throw new AxisError(e.getLocalizedMessage()+": "+e.getRootThrowable().getLocalizedMessage(), e.getRootThrowable());
                }
        }
     
    }
    