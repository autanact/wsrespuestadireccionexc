/*
 * Generated by XDoclet - Do not edit!
 */
package co.net.une.www.interfaces;

/**
 * Local home interface for WSRespuestaDireccionExc.
 * @xdoclet-generated at ${TODAY}
 * @copyright The XDoclet Team
 * @author XDoclet
 * @version ${version}
 */
public interface WSRespuestaDireccionExcLocalHome
   extends javax.ejb.EJBLocalHome
{
   public static final String COMP_NAME="java:comp/env/ejb/WSRespuestaDireccionExcLocal";
   public static final String JNDI_NAME="ejb/WSRespuestaDireccionExcLocal";

   public co.net.une.www.interfaces.WSRespuestaDireccionExcLocal create()
      throws javax.ejb.CreateException;

}
