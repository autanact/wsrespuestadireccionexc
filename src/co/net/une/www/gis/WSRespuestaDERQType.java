
/**
 * WSRespuestaDERQType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:41 LKT)
 */
            
                package co.net.une.www.gis;
            

            /**
            *  WSRespuestaDERQType bean class
            */
        
        public  class WSRespuestaDERQType
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = WSRespuestaDE-RQ-Type
                Namespace URI = http://www.une.net.co/gis
                Namespace Prefix = ns1
                */
            

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("http://www.une.net.co/gis")){
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        

                        /**
                        * field for CodigoSolicitud
                        */

                        
                                    protected co.net.une.www.gis.BoundedString15 localCodigoSolicitud ;
                                

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString15
                           */
                           public  co.net.une.www.gis.BoundedString15 getCodigoSolicitud(){
                               return localCodigoSolicitud;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CodigoSolicitud
                               */
                               public void setCodigoSolicitud(co.net.une.www.gis.BoundedString15 param){
                            
                                            this.localCodigoSolicitud=param;
                                    

                               }
                            

                        /**
                        * field for CodigoSistema
                        */

                        
                                    protected co.net.une.www.gis.BoundedString30 localCodigoSistema ;
                                

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString30
                           */
                           public  co.net.une.www.gis.BoundedString30 getCodigoSistema(){
                               return localCodigoSistema;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CodigoSistema
                               */
                               public void setCodigoSistema(co.net.une.www.gis.BoundedString30 param){
                            
                                            this.localCodigoSistema=param;
                                    

                               }
                            

                        /**
                        * field for EstadoExcepcion
                        */

                        
                                    protected co.net.une.www.gis.BoundedString30 localEstadoExcepcion ;
                                

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString30
                           */
                           public  co.net.une.www.gis.BoundedString30 getEstadoExcepcion(){
                               return localEstadoExcepcion;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EstadoExcepcion
                               */
                               public void setEstadoExcepcion(co.net.une.www.gis.BoundedString30 param){
                            
                                            this.localEstadoExcepcion=param;
                                    

                               }
                            

                        /**
                        * field for EstadoPagina
                        */

                        
                                    protected co.net.une.www.gis.BoundedString5 localEstadoPagina ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEstadoPaginaTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString5
                           */
                           public  co.net.une.www.gis.BoundedString5 getEstadoPagina(){
                               return localEstadoPagina;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EstadoPagina
                               */
                               public void setEstadoPagina(co.net.une.www.gis.BoundedString5 param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localEstadoPaginaTracker = true;
                                       } else {
                                          localEstadoPaginaTracker = false;
                                              
                                       }
                                   
                                            this.localEstadoPagina=param;
                                    

                               }
                            

                        /**
                        * field for Instalacion
                        */

                        
                                    protected co.net.une.www.gis.BoundedString18 localInstalacion ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInstalacionTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString18
                           */
                           public  co.net.une.www.gis.BoundedString18 getInstalacion(){
                               return localInstalacion;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Instalacion
                               */
                               public void setInstalacion(co.net.une.www.gis.BoundedString18 param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localInstalacionTracker = true;
                                       } else {
                                          localInstalacionTracker = false;
                                              
                                       }
                                   
                                            this.localInstalacion=param;
                                    

                               }
                            

                        /**
                        * field for CodigoDireccion
                        */

                        
                                    protected co.net.une.www.gis.BoundedString100 localCodigoDireccion ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCodigoDireccionTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString100
                           */
                           public  co.net.une.www.gis.BoundedString100 getCodigoDireccion(){
                               return localCodigoDireccion;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CodigoDireccion
                               */
                               public void setCodigoDireccion(co.net.une.www.gis.BoundedString100 param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localCodigoDireccionTracker = true;
                                       } else {
                                          localCodigoDireccionTracker = false;
                                              
                                       }
                                   
                                            this.localCodigoDireccion=param;
                                    

                               }
                            

                        /**
                        * field for DireccionNormalizada
                        */

                        
                                    protected co.net.une.www.gis.BoundedString250 localDireccionNormalizada ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDireccionNormalizadaTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString250
                           */
                           public  co.net.une.www.gis.BoundedString250 getDireccionNormalizada(){
                               return localDireccionNormalizada;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DireccionNormalizada
                               */
                               public void setDireccionNormalizada(co.net.une.www.gis.BoundedString250 param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localDireccionNormalizadaTracker = true;
                                       } else {
                                          localDireccionNormalizadaTracker = false;
                                              
                                       }
                                   
                                            this.localDireccionNormalizada=param;
                                    

                               }
                            

                        /**
                        * field for DireccionAnterior
                        */

                        
                                    protected co.net.une.www.gis.BoundedString250 localDireccionAnterior ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDireccionAnteriorTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString250
                           */
                           public  co.net.une.www.gis.BoundedString250 getDireccionAnterior(){
                               return localDireccionAnterior;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DireccionAnterior
                               */
                               public void setDireccionAnterior(co.net.une.www.gis.BoundedString250 param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localDireccionAnteriorTracker = true;
                                       } else {
                                          localDireccionAnteriorTracker = false;
                                              
                                       }
                                   
                                            this.localDireccionAnterior=param;
                                    

                               }
                            

                        /**
                        * field for Placa
                        */

                        
                                    protected co.net.une.www.gis.BoundedString10 localPlaca ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPlacaTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString10
                           */
                           public  co.net.une.www.gis.BoundedString10 getPlaca(){
                               return localPlaca;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Placa
                               */
                               public void setPlaca(co.net.une.www.gis.BoundedString10 param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localPlacaTracker = true;
                                       } else {
                                          localPlacaTracker = false;
                                              
                                       }
                                   
                                            this.localPlaca=param;
                                    

                               }
                            

                        /**
                        * field for Agregado
                        */

                        
                                    protected co.net.une.www.gis.BoundedString100 localAgregado ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAgregadoTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString100
                           */
                           public  co.net.une.www.gis.BoundedString100 getAgregado(){
                               return localAgregado;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Agregado
                               */
                               public void setAgregado(co.net.une.www.gis.BoundedString100 param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localAgregadoTracker = true;
                                       } else {
                                          localAgregadoTracker = false;
                                              
                                       }
                                   
                                            this.localAgregado=param;
                                    

                               }
                            

                        /**
                        * field for Remanente
                        */

                        
                                    protected co.net.une.www.gis.BoundedString100 localRemanente ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRemanenteTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString100
                           */
                           public  co.net.une.www.gis.BoundedString100 getRemanente(){
                               return localRemanente;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Remanente
                               */
                               public void setRemanente(co.net.une.www.gis.BoundedString100 param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localRemanenteTracker = true;
                                       } else {
                                          localRemanenteTracker = false;
                                              
                                       }
                                   
                                            this.localRemanente=param;
                                    

                               }
                            

                        /**
                        * field for Latitud
                        */

                        
                                    protected java.lang.String localLatitud ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLatitudTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getLatitud(){
                               return localLatitud;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Latitud
                               */
                               public void setLatitud(java.lang.String param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localLatitudTracker = true;
                                       } else {
                                          localLatitudTracker = false;
                                              
                                       }
                                   
                                            this.localLatitud=param;
                                    

                               }
                            

                        /**
                        * field for Longitud
                        */

                        
                                    protected java.lang.String localLongitud ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLongitudTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getLongitud(){
                               return localLongitud;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Longitud
                               */
                               public void setLongitud(java.lang.String param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localLongitudTracker = true;
                                       } else {
                                          localLongitudTracker = false;
                                              
                                       }
                                   
                                            this.localLongitud=param;
                                    

                               }
                            

                        /**
                        * field for CoordenadaX
                        */

                        
                                    protected java.lang.String localCoordenadaX ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCoordenadaXTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCoordenadaX(){
                               return localCoordenadaX;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CoordenadaX
                               */
                               public void setCoordenadaX(java.lang.String param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localCoordenadaXTracker = true;
                                       } else {
                                          localCoordenadaXTracker = false;
                                              
                                       }
                                   
                                            this.localCoordenadaX=param;
                                    

                               }
                            

                        /**
                        * field for CoordenadaY
                        */

                        
                                    protected java.lang.String localCoordenadaY ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCoordenadaYTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCoordenadaY(){
                               return localCoordenadaY;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CoordenadaY
                               */
                               public void setCoordenadaY(java.lang.String param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localCoordenadaYTracker = true;
                                       } else {
                                          localCoordenadaYTracker = false;
                                              
                                       }
                                   
                                            this.localCoordenadaY=param;
                                    

                               }
                            

                        /**
                        * field for Estrato
                        */

                        
                                    protected java.math.BigInteger localEstrato ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEstratoTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.math.BigInteger
                           */
                           public  java.math.BigInteger getEstrato(){
                               return localEstrato;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Estrato
                               */
                               public void setEstrato(java.math.BigInteger param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localEstratoTracker = true;
                                       } else {
                                          localEstratoTracker = false;
                                              
                                       }
                                   
                                            this.localEstrato=param;
                                    

                               }
                            

                        /**
                        * field for Rural
                        */

                        
                                    protected co.net.une.www.gis.BoundedString2 localRural ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRuralTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString2
                           */
                           public  co.net.une.www.gis.BoundedString2 getRural(){
                               return localRural;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Rural
                               */
                               public void setRural(co.net.une.www.gis.BoundedString2 param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localRuralTracker = true;
                                       } else {
                                          localRuralTracker = false;
                                              
                                       }
                                   
                                            this.localRural=param;
                                    

                               }
                            

                        /**
                        * field for EstadoGeoreferenciacion
                        */

                        
                                    protected co.net.une.www.gis.BoundedString1 localEstadoGeoreferenciacion ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEstadoGeoreferenciacionTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString1
                           */
                           public  co.net.une.www.gis.BoundedString1 getEstadoGeoreferenciacion(){
                               return localEstadoGeoreferenciacion;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EstadoGeoreferenciacion
                               */
                               public void setEstadoGeoreferenciacion(co.net.une.www.gis.BoundedString1 param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localEstadoGeoreferenciacionTracker = true;
                                       } else {
                                          localEstadoGeoreferenciacionTracker = false;
                                              
                                       }
                                   
                                            this.localEstadoGeoreferenciacion=param;
                                    

                               }
                            

                        /**
                        * field for CodigoDireccionProveevor
                        */

                        
                                    protected co.net.une.www.gis.BoundedString100 localCodigoDireccionProveevor ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCodigoDireccionProveevorTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString100
                           */
                           public  co.net.une.www.gis.BoundedString100 getCodigoDireccionProveevor(){
                               return localCodigoDireccionProveevor;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CodigoDireccionProveevor
                               */
                               public void setCodigoDireccionProveevor(co.net.une.www.gis.BoundedString100 param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localCodigoDireccionProveevorTracker = true;
                                       } else {
                                          localCodigoDireccionProveevorTracker = false;
                                              
                                       }
                                   
                                            this.localCodigoDireccionProveevor=param;
                                    

                               }
                            

                        /**
                        * field for CodigoPais
                        */

                        
                                    protected co.net.une.www.gis.BoundedString2 localCodigoPais ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCodigoPaisTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString2
                           */
                           public  co.net.une.www.gis.BoundedString2 getCodigoPais(){
                               return localCodigoPais;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CodigoPais
                               */
                               public void setCodigoPais(co.net.une.www.gis.BoundedString2 param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localCodigoPaisTracker = true;
                                       } else {
                                          localCodigoPaisTracker = false;
                                              
                                       }
                                   
                                            this.localCodigoPais=param;
                                    

                               }
                            

                        /**
                        * field for CodigoDepartamento
                        */

                        
                                    protected co.net.une.www.gis.BoundedString2 localCodigoDepartamento ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCodigoDepartamentoTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString2
                           */
                           public  co.net.une.www.gis.BoundedString2 getCodigoDepartamento(){
                               return localCodigoDepartamento;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CodigoDepartamento
                               */
                               public void setCodigoDepartamento(co.net.une.www.gis.BoundedString2 param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localCodigoDepartamentoTracker = true;
                                       } else {
                                          localCodigoDepartamentoTracker = false;
                                              
                                       }
                                   
                                            this.localCodigoDepartamento=param;
                                    

                               }
                            

                        /**
                        * field for CodigoMunicipio
                        */

                        
                                    protected co.net.une.www.gis.BoundedString8 localCodigoMunicipio ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCodigoMunicipioTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString8
                           */
                           public  co.net.une.www.gis.BoundedString8 getCodigoMunicipio(){
                               return localCodigoMunicipio;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CodigoMunicipio
                               */
                               public void setCodigoMunicipio(co.net.une.www.gis.BoundedString8 param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localCodigoMunicipioTracker = true;
                                       } else {
                                          localCodigoMunicipioTracker = false;
                                              
                                       }
                                   
                                            this.localCodigoMunicipio=param;
                                    

                               }
                            

                        /**
                        * field for CodigoComuna
                        */

                        
                                    protected co.net.une.www.gis.BoundedString10 localCodigoComuna ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCodigoComunaTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString10
                           */
                           public  co.net.une.www.gis.BoundedString10 getCodigoComuna(){
                               return localCodigoComuna;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CodigoComuna
                               */
                               public void setCodigoComuna(co.net.une.www.gis.BoundedString10 param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localCodigoComunaTracker = true;
                                       } else {
                                          localCodigoComunaTracker = false;
                                              
                                       }
                                   
                                            this.localCodigoComuna=param;
                                    

                               }
                            

                        /**
                        * field for CodigoBarrio
                        */

                        
                                    protected co.net.une.www.gis.BoundedString10 localCodigoBarrio ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCodigoBarrioTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString10
                           */
                           public  co.net.une.www.gis.BoundedString10 getCodigoBarrio(){
                               return localCodigoBarrio;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CodigoBarrio
                               */
                               public void setCodigoBarrio(co.net.une.www.gis.BoundedString10 param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localCodigoBarrioTracker = true;
                                       } else {
                                          localCodigoBarrioTracker = false;
                                              
                                       }
                                   
                                            this.localCodigoBarrio=param;
                                    

                               }
                            

                        /**
                        * field for CodigoLocalizacionTipo1
                        */

                        
                                    protected co.net.une.www.gis.BoundedString10 localCodigoLocalizacionTipo1 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCodigoLocalizacionTipo1Tracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString10
                           */
                           public  co.net.une.www.gis.BoundedString10 getCodigoLocalizacionTipo1(){
                               return localCodigoLocalizacionTipo1;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CodigoLocalizacionTipo1
                               */
                               public void setCodigoLocalizacionTipo1(co.net.une.www.gis.BoundedString10 param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localCodigoLocalizacionTipo1Tracker = true;
                                       } else {
                                          localCodigoLocalizacionTipo1Tracker = false;
                                              
                                       }
                                   
                                            this.localCodigoLocalizacionTipo1=param;
                                    

                               }
                            

                        /**
                        * field for CodigoDaneManzana
                        */

                        
                                    protected co.net.une.www.gis.BoundedString14 localCodigoDaneManzana ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCodigoDaneManzanaTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString14
                           */
                           public  co.net.une.www.gis.BoundedString14 getCodigoDaneManzana(){
                               return localCodigoDaneManzana;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CodigoDaneManzana
                               */
                               public void setCodigoDaneManzana(co.net.une.www.gis.BoundedString14 param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localCodigoDaneManzanaTracker = true;
                                       } else {
                                          localCodigoDaneManzanaTracker = false;
                                              
                                       }
                                   
                                            this.localCodigoDaneManzana=param;
                                    

                               }
                            

                        /**
                        * field for CodigoPredio
                        */

                        
                                    protected co.net.une.www.gis.BoundedString17 localCodigoPredio ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCodigoPredioTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString17
                           */
                           public  co.net.une.www.gis.BoundedString17 getCodigoPredio(){
                               return localCodigoPredio;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CodigoPredio
                               */
                               public void setCodigoPredio(co.net.une.www.gis.BoundedString17 param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localCodigoPredioTracker = true;
                                       } else {
                                          localCodigoPredioTracker = false;
                                              
                                       }
                                   
                                            this.localCodigoPredio=param;
                                    

                               }
                            

                        /**
                        * field for TipoAgregacionNivel1
                        */

                        
                                    protected co.net.une.www.gis.BoundedString20 localTipoAgregacionNivel1 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTipoAgregacionNivel1Tracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString20
                           */
                           public  co.net.une.www.gis.BoundedString20 getTipoAgregacionNivel1(){
                               return localTipoAgregacionNivel1;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TipoAgregacionNivel1
                               */
                               public void setTipoAgregacionNivel1(co.net.une.www.gis.BoundedString20 param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localTipoAgregacionNivel1Tracker = true;
                                       } else {
                                          localTipoAgregacionNivel1Tracker = false;
                                              
                                       }
                                   
                                            this.localTipoAgregacionNivel1=param;
                                    

                               }
                            

                        /**
                        * field for NombreLocalizacionTipo1
                        */

                        
                                    protected co.net.une.www.gis.BoundedString100 localNombreLocalizacionTipo1 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNombreLocalizacionTipo1Tracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString100
                           */
                           public  co.net.une.www.gis.BoundedString100 getNombreLocalizacionTipo1(){
                               return localNombreLocalizacionTipo1;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param NombreLocalizacionTipo1
                               */
                               public void setNombreLocalizacionTipo1(co.net.une.www.gis.BoundedString100 param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localNombreLocalizacionTipo1Tracker = true;
                                       } else {
                                          localNombreLocalizacionTipo1Tracker = false;
                                              
                                       }
                                   
                                            this.localNombreLocalizacionTipo1=param;
                                    

                               }
                            

                        /**
                        * field for NombreBarrio
                        */

                        
                                    protected co.net.une.www.gis.BoundedString100 localNombreBarrio ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNombreBarrioTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString100
                           */
                           public  co.net.une.www.gis.BoundedString100 getNombreBarrio(){
                               return localNombreBarrio;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param NombreBarrio
                               */
                               public void setNombreBarrio(co.net.une.www.gis.BoundedString100 param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localNombreBarrioTracker = true;
                                       } else {
                                          localNombreBarrioTracker = false;
                                              
                                       }
                                   
                                            this.localNombreBarrio=param;
                                    

                               }
                            

                        /**
                        * field for NombreComuna
                        */

                        
                                    protected co.net.une.www.gis.BoundedString100 localNombreComuna ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNombreComunaTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString100
                           */
                           public  co.net.une.www.gis.BoundedString100 getNombreComuna(){
                               return localNombreComuna;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param NombreComuna
                               */
                               public void setNombreComuna(co.net.une.www.gis.BoundedString100 param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localNombreComunaTracker = true;
                                       } else {
                                          localNombreComunaTracker = false;
                                              
                                       }
                                   
                                            this.localNombreComuna=param;
                                    

                               }
                            

                        /**
                        * field for DireccionEstandarNLectura
                        */

                        
                                    protected co.net.une.www.gis.BoundedString250 localDireccionEstandarNLectura ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDireccionEstandarNLecturaTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString250
                           */
                           public  co.net.une.www.gis.BoundedString250 getDireccionEstandarNLectura(){
                               return localDireccionEstandarNLectura;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DireccionEstandarNLectura
                               */
                               public void setDireccionEstandarNLectura(co.net.une.www.gis.BoundedString250 param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localDireccionEstandarNLecturaTracker = true;
                                       } else {
                                          localDireccionEstandarNLecturaTracker = false;
                                              
                                       }
                                   
                                            this.localDireccionEstandarNLectura=param;
                                    

                               }
                            

     /**
     * isReaderMTOMAware
     * @return true if the reader supports MTOM
     */
   public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
        boolean isReaderMTOMAware = false;
        
        try{
          isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader.getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
        }catch(java.lang.IllegalArgumentException e){
          isReaderMTOMAware = false;
        }
        return isReaderMTOMAware;
   }
     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName){

                 public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
                       WSRespuestaDERQType.this.serialize(parentQName,factory,xmlWriter);
                 }
               };
               return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(
               parentQName,factory,dataSource);
            
       }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       final org.apache.axiom.om.OMFactory factory,
                                       org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,factory,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               final org.apache.axiom.om.OMFactory factory,
                               org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();

                    if ((namespace != null) && (namespace.trim().length() > 0)) {
                        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                        if (writerPrefix != null) {
                            xmlWriter.writeStartElement(namespace, parentQName.getLocalPart());
                        } else {
                            if (prefix == null) {
                                prefix = generatePrefix(namespace);
                            }

                            xmlWriter.writeStartElement(prefix, parentQName.getLocalPart(), namespace);
                            xmlWriter.writeNamespace(prefix, namespace);
                            xmlWriter.setPrefix(prefix, namespace);
                        }
                    } else {
                        xmlWriter.writeStartElement(parentQName.getLocalPart());
                    }
                
                  if (serializeType){
               

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"http://www.une.net.co/gis");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":WSRespuestaDE-RQ-Type",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "WSRespuestaDE-RQ-Type",
                           xmlWriter);
                   }

               
                   }
               
                                            if (localCodigoSolicitud==null){
                                                 throw new org.apache.axis2.databinding.ADBException("codigoSolicitud cannot be null!!");
                                            }
                                           localCodigoSolicitud.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","codigoSolicitud"),
                                               factory,xmlWriter);
                                        
                                            if (localCodigoSistema==null){
                                                 throw new org.apache.axis2.databinding.ADBException("codigoSistema cannot be null!!");
                                            }
                                           localCodigoSistema.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","codigoSistema"),
                                               factory,xmlWriter);
                                        
                                            if (localEstadoExcepcion==null){
                                                 throw new org.apache.axis2.databinding.ADBException("estadoExcepcion cannot be null!!");
                                            }
                                           localEstadoExcepcion.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","estadoExcepcion"),
                                               factory,xmlWriter);
                                         if (localEstadoPaginaTracker){
                                            if (localEstadoPagina==null){
                                                 throw new org.apache.axis2.databinding.ADBException("estadoPagina cannot be null!!");
                                            }
                                           localEstadoPagina.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","estadoPagina"),
                                               factory,xmlWriter);
                                        } if (localInstalacionTracker){
                                            if (localInstalacion==null){
                                                 throw new org.apache.axis2.databinding.ADBException("instalacion cannot be null!!");
                                            }
                                           localInstalacion.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","instalacion"),
                                               factory,xmlWriter);
                                        } if (localCodigoDireccionTracker){
                                            if (localCodigoDireccion==null){
                                                 throw new org.apache.axis2.databinding.ADBException("codigoDireccion cannot be null!!");
                                            }
                                           localCodigoDireccion.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","codigoDireccion"),
                                               factory,xmlWriter);
                                        } if (localDireccionNormalizadaTracker){
                                            if (localDireccionNormalizada==null){
                                                 throw new org.apache.axis2.databinding.ADBException("direccionNormalizada cannot be null!!");
                                            }
                                           localDireccionNormalizada.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","direccionNormalizada"),
                                               factory,xmlWriter);
                                        } if (localDireccionAnteriorTracker){
                                            if (localDireccionAnterior==null){
                                                 throw new org.apache.axis2.databinding.ADBException("direccionAnterior cannot be null!!");
                                            }
                                           localDireccionAnterior.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","direccionAnterior"),
                                               factory,xmlWriter);
                                        } if (localPlacaTracker){
                                            if (localPlaca==null){
                                                 throw new org.apache.axis2.databinding.ADBException("placa cannot be null!!");
                                            }
                                           localPlaca.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","placa"),
                                               factory,xmlWriter);
                                        } if (localAgregadoTracker){
                                            if (localAgregado==null){
                                                 throw new org.apache.axis2.databinding.ADBException("agregado cannot be null!!");
                                            }
                                           localAgregado.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","agregado"),
                                               factory,xmlWriter);
                                        } if (localRemanenteTracker){
                                            if (localRemanente==null){
                                                 throw new org.apache.axis2.databinding.ADBException("remanente cannot be null!!");
                                            }
                                           localRemanente.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","remanente"),
                                               factory,xmlWriter);
                                        } if (localLatitudTracker){
                                    namespace = "http://www.une.net.co/gis";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"latitud", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"latitud");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("latitud");
                                    }
                                

                                          if (localLatitud==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("latitud cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localLatitud);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLongitudTracker){
                                    namespace = "http://www.une.net.co/gis";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"longitud", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"longitud");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("longitud");
                                    }
                                

                                          if (localLongitud==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("longitud cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localLongitud);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCoordenadaXTracker){
                                    namespace = "http://www.une.net.co/gis";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"coordenadaX", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"coordenadaX");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("coordenadaX");
                                    }
                                

                                          if (localCoordenadaX==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("coordenadaX cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCoordenadaX);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCoordenadaYTracker){
                                    namespace = "http://www.une.net.co/gis";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"coordenadaY", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"coordenadaY");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("coordenadaY");
                                    }
                                

                                          if (localCoordenadaY==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("coordenadaY cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCoordenadaY);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localEstratoTracker){
                                    namespace = "http://www.une.net.co/gis";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"estrato", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"estrato");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("estrato");
                                    }
                                

                                          if (localEstrato==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("estrato cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEstrato));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRuralTracker){
                                            if (localRural==null){
                                                 throw new org.apache.axis2.databinding.ADBException("rural cannot be null!!");
                                            }
                                           localRural.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","rural"),
                                               factory,xmlWriter);
                                        } if (localEstadoGeoreferenciacionTracker){
                                            if (localEstadoGeoreferenciacion==null){
                                                 throw new org.apache.axis2.databinding.ADBException("estadoGeoreferenciacion cannot be null!!");
                                            }
                                           localEstadoGeoreferenciacion.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","estadoGeoreferenciacion"),
                                               factory,xmlWriter);
                                        } if (localCodigoDireccionProveevorTracker){
                                            if (localCodigoDireccionProveevor==null){
                                                 throw new org.apache.axis2.databinding.ADBException("codigoDireccionProveevor cannot be null!!");
                                            }
                                           localCodigoDireccionProveevor.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","codigoDireccionProveevor"),
                                               factory,xmlWriter);
                                        } if (localCodigoPaisTracker){
                                            if (localCodigoPais==null){
                                                 throw new org.apache.axis2.databinding.ADBException("codigoPais cannot be null!!");
                                            }
                                           localCodigoPais.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","codigoPais"),
                                               factory,xmlWriter);
                                        } if (localCodigoDepartamentoTracker){
                                            if (localCodigoDepartamento==null){
                                                 throw new org.apache.axis2.databinding.ADBException("codigoDepartamento cannot be null!!");
                                            }
                                           localCodigoDepartamento.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","codigoDepartamento"),
                                               factory,xmlWriter);
                                        } if (localCodigoMunicipioTracker){
                                            if (localCodigoMunicipio==null){
                                                 throw new org.apache.axis2.databinding.ADBException("codigoMunicipio cannot be null!!");
                                            }
                                           localCodigoMunicipio.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","codigoMunicipio"),
                                               factory,xmlWriter);
                                        } if (localCodigoComunaTracker){
                                            if (localCodigoComuna==null){
                                                 throw new org.apache.axis2.databinding.ADBException("codigoComuna cannot be null!!");
                                            }
                                           localCodigoComuna.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","codigoComuna"),
                                               factory,xmlWriter);
                                        } if (localCodigoBarrioTracker){
                                            if (localCodigoBarrio==null){
                                                 throw new org.apache.axis2.databinding.ADBException("codigoBarrio cannot be null!!");
                                            }
                                           localCodigoBarrio.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","codigoBarrio"),
                                               factory,xmlWriter);
                                        } if (localCodigoLocalizacionTipo1Tracker){
                                            if (localCodigoLocalizacionTipo1==null){
                                                 throw new org.apache.axis2.databinding.ADBException("codigoLocalizacionTipo1 cannot be null!!");
                                            }
                                           localCodigoLocalizacionTipo1.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","codigoLocalizacionTipo1"),
                                               factory,xmlWriter);
                                        } if (localCodigoDaneManzanaTracker){
                                            if (localCodigoDaneManzana==null){
                                                 throw new org.apache.axis2.databinding.ADBException("codigoDaneManzana cannot be null!!");
                                            }
                                           localCodigoDaneManzana.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","codigoDaneManzana"),
                                               factory,xmlWriter);
                                        } if (localCodigoPredioTracker){
                                            if (localCodigoPredio==null){
                                                 throw new org.apache.axis2.databinding.ADBException("codigoPredio cannot be null!!");
                                            }
                                           localCodigoPredio.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","codigoPredio"),
                                               factory,xmlWriter);
                                        } if (localTipoAgregacionNivel1Tracker){
                                            if (localTipoAgregacionNivel1==null){
                                                 throw new org.apache.axis2.databinding.ADBException("tipoAgregacionNivel1 cannot be null!!");
                                            }
                                           localTipoAgregacionNivel1.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","tipoAgregacionNivel1"),
                                               factory,xmlWriter);
                                        } if (localNombreLocalizacionTipo1Tracker){
                                            if (localNombreLocalizacionTipo1==null){
                                                 throw new org.apache.axis2.databinding.ADBException("nombreLocalizacionTipo1 cannot be null!!");
                                            }
                                           localNombreLocalizacionTipo1.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","nombreLocalizacionTipo1"),
                                               factory,xmlWriter);
                                        } if (localNombreBarrioTracker){
                                            if (localNombreBarrio==null){
                                                 throw new org.apache.axis2.databinding.ADBException("nombreBarrio cannot be null!!");
                                            }
                                           localNombreBarrio.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","nombreBarrio"),
                                               factory,xmlWriter);
                                        } if (localNombreComunaTracker){
                                            if (localNombreComuna==null){
                                                 throw new org.apache.axis2.databinding.ADBException("nombreComuna cannot be null!!");
                                            }
                                           localNombreComuna.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","nombreComuna"),
                                               factory,xmlWriter);
                                        } if (localDireccionEstandarNLecturaTracker){
                                            if (localDireccionEstandarNLectura==null){
                                                 throw new org.apache.axis2.databinding.ADBException("direccionEstandarNLectura cannot be null!!");
                                            }
                                           localDireccionEstandarNLectura.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","direccionEstandarNLectura"),
                                               factory,xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

         /**
          * Util method to write an attribute with the ns prefix
          */
          private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                      java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
              if (xmlWriter.getPrefix(namespace) == null) {
                       xmlWriter.writeNamespace(prefix, namespace);
                       xmlWriter.setPrefix(prefix, namespace);

              }

              xmlWriter.writeAttribute(namespace,attName,attValue);

         }

        /**
          * Util method to write an attribute without the ns prefix
          */
          private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                      java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
                if (namespace.equals(""))
              {
                  xmlWriter.writeAttribute(attName,attValue);
              }
              else
              {
                  registerPrefix(xmlWriter, namespace);
                  xmlWriter.writeAttribute(namespace,attName,attValue);
              }
          }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


         /**
         * Register a namespace prefix
         */
         private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
                java.lang.String prefix = xmlWriter.getPrefix(namespace);

                if (prefix == null) {
                    prefix = generatePrefix(namespace);

                    while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                    }

                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }

                return prefix;
            }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "codigoSolicitud"));
                            
                            
                                    if (localCodigoSolicitud==null){
                                         throw new org.apache.axis2.databinding.ADBException("codigoSolicitud cannot be null!!");
                                    }
                                    elementList.add(localCodigoSolicitud);
                                
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "codigoSistema"));
                            
                            
                                    if (localCodigoSistema==null){
                                         throw new org.apache.axis2.databinding.ADBException("codigoSistema cannot be null!!");
                                    }
                                    elementList.add(localCodigoSistema);
                                
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "estadoExcepcion"));
                            
                            
                                    if (localEstadoExcepcion==null){
                                         throw new org.apache.axis2.databinding.ADBException("estadoExcepcion cannot be null!!");
                                    }
                                    elementList.add(localEstadoExcepcion);
                                 if (localEstadoPaginaTracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "estadoPagina"));
                            
                            
                                    if (localEstadoPagina==null){
                                         throw new org.apache.axis2.databinding.ADBException("estadoPagina cannot be null!!");
                                    }
                                    elementList.add(localEstadoPagina);
                                } if (localInstalacionTracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "instalacion"));
                            
                            
                                    if (localInstalacion==null){
                                         throw new org.apache.axis2.databinding.ADBException("instalacion cannot be null!!");
                                    }
                                    elementList.add(localInstalacion);
                                } if (localCodigoDireccionTracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "codigoDireccion"));
                            
                            
                                    if (localCodigoDireccion==null){
                                         throw new org.apache.axis2.databinding.ADBException("codigoDireccion cannot be null!!");
                                    }
                                    elementList.add(localCodigoDireccion);
                                } if (localDireccionNormalizadaTracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "direccionNormalizada"));
                            
                            
                                    if (localDireccionNormalizada==null){
                                         throw new org.apache.axis2.databinding.ADBException("direccionNormalizada cannot be null!!");
                                    }
                                    elementList.add(localDireccionNormalizada);
                                } if (localDireccionAnteriorTracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "direccionAnterior"));
                            
                            
                                    if (localDireccionAnterior==null){
                                         throw new org.apache.axis2.databinding.ADBException("direccionAnterior cannot be null!!");
                                    }
                                    elementList.add(localDireccionAnterior);
                                } if (localPlacaTracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "placa"));
                            
                            
                                    if (localPlaca==null){
                                         throw new org.apache.axis2.databinding.ADBException("placa cannot be null!!");
                                    }
                                    elementList.add(localPlaca);
                                } if (localAgregadoTracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "agregado"));
                            
                            
                                    if (localAgregado==null){
                                         throw new org.apache.axis2.databinding.ADBException("agregado cannot be null!!");
                                    }
                                    elementList.add(localAgregado);
                                } if (localRemanenteTracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "remanente"));
                            
                            
                                    if (localRemanente==null){
                                         throw new org.apache.axis2.databinding.ADBException("remanente cannot be null!!");
                                    }
                                    elementList.add(localRemanente);
                                } if (localLatitudTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "latitud"));
                                 
                                        if (localLatitud != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLatitud));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("latitud cannot be null!!");
                                        }
                                    } if (localLongitudTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "longitud"));
                                 
                                        if (localLongitud != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLongitud));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("longitud cannot be null!!");
                                        }
                                    } if (localCoordenadaXTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "coordenadaX"));
                                 
                                        if (localCoordenadaX != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCoordenadaX));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("coordenadaX cannot be null!!");
                                        }
                                    } if (localCoordenadaYTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "coordenadaY"));
                                 
                                        if (localCoordenadaY != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCoordenadaY));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("coordenadaY cannot be null!!");
                                        }
                                    } if (localEstratoTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "estrato"));
                                 
                                        if (localEstrato != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEstrato));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("estrato cannot be null!!");
                                        }
                                    } if (localRuralTracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "rural"));
                            
                            
                                    if (localRural==null){
                                         throw new org.apache.axis2.databinding.ADBException("rural cannot be null!!");
                                    }
                                    elementList.add(localRural);
                                } if (localEstadoGeoreferenciacionTracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "estadoGeoreferenciacion"));
                            
                            
                                    if (localEstadoGeoreferenciacion==null){
                                         throw new org.apache.axis2.databinding.ADBException("estadoGeoreferenciacion cannot be null!!");
                                    }
                                    elementList.add(localEstadoGeoreferenciacion);
                                } if (localCodigoDireccionProveevorTracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "codigoDireccionProveevor"));
                            
                            
                                    if (localCodigoDireccionProveevor==null){
                                         throw new org.apache.axis2.databinding.ADBException("codigoDireccionProveevor cannot be null!!");
                                    }
                                    elementList.add(localCodigoDireccionProveevor);
                                } if (localCodigoPaisTracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "codigoPais"));
                            
                            
                                    if (localCodigoPais==null){
                                         throw new org.apache.axis2.databinding.ADBException("codigoPais cannot be null!!");
                                    }
                                    elementList.add(localCodigoPais);
                                } if (localCodigoDepartamentoTracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "codigoDepartamento"));
                            
                            
                                    if (localCodigoDepartamento==null){
                                         throw new org.apache.axis2.databinding.ADBException("codigoDepartamento cannot be null!!");
                                    }
                                    elementList.add(localCodigoDepartamento);
                                } if (localCodigoMunicipioTracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "codigoMunicipio"));
                            
                            
                                    if (localCodigoMunicipio==null){
                                         throw new org.apache.axis2.databinding.ADBException("codigoMunicipio cannot be null!!");
                                    }
                                    elementList.add(localCodigoMunicipio);
                                } if (localCodigoComunaTracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "codigoComuna"));
                            
                            
                                    if (localCodigoComuna==null){
                                         throw new org.apache.axis2.databinding.ADBException("codigoComuna cannot be null!!");
                                    }
                                    elementList.add(localCodigoComuna);
                                } if (localCodigoBarrioTracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "codigoBarrio"));
                            
                            
                                    if (localCodigoBarrio==null){
                                         throw new org.apache.axis2.databinding.ADBException("codigoBarrio cannot be null!!");
                                    }
                                    elementList.add(localCodigoBarrio);
                                } if (localCodigoLocalizacionTipo1Tracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "codigoLocalizacionTipo1"));
                            
                            
                                    if (localCodigoLocalizacionTipo1==null){
                                         throw new org.apache.axis2.databinding.ADBException("codigoLocalizacionTipo1 cannot be null!!");
                                    }
                                    elementList.add(localCodigoLocalizacionTipo1);
                                } if (localCodigoDaneManzanaTracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "codigoDaneManzana"));
                            
                            
                                    if (localCodigoDaneManzana==null){
                                         throw new org.apache.axis2.databinding.ADBException("codigoDaneManzana cannot be null!!");
                                    }
                                    elementList.add(localCodigoDaneManzana);
                                } if (localCodigoPredioTracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "codigoPredio"));
                            
                            
                                    if (localCodigoPredio==null){
                                         throw new org.apache.axis2.databinding.ADBException("codigoPredio cannot be null!!");
                                    }
                                    elementList.add(localCodigoPredio);
                                } if (localTipoAgregacionNivel1Tracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "tipoAgregacionNivel1"));
                            
                            
                                    if (localTipoAgregacionNivel1==null){
                                         throw new org.apache.axis2.databinding.ADBException("tipoAgregacionNivel1 cannot be null!!");
                                    }
                                    elementList.add(localTipoAgregacionNivel1);
                                } if (localNombreLocalizacionTipo1Tracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "nombreLocalizacionTipo1"));
                            
                            
                                    if (localNombreLocalizacionTipo1==null){
                                         throw new org.apache.axis2.databinding.ADBException("nombreLocalizacionTipo1 cannot be null!!");
                                    }
                                    elementList.add(localNombreLocalizacionTipo1);
                                } if (localNombreBarrioTracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "nombreBarrio"));
                            
                            
                                    if (localNombreBarrio==null){
                                         throw new org.apache.axis2.databinding.ADBException("nombreBarrio cannot be null!!");
                                    }
                                    elementList.add(localNombreBarrio);
                                } if (localNombreComunaTracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "nombreComuna"));
                            
                            
                                    if (localNombreComuna==null){
                                         throw new org.apache.axis2.databinding.ADBException("nombreComuna cannot be null!!");
                                    }
                                    elementList.add(localNombreComuna);
                                } if (localDireccionEstandarNLecturaTracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "direccionEstandarNLectura"));
                            
                            
                                    if (localDireccionEstandarNLectura==null){
                                         throw new org.apache.axis2.databinding.ADBException("direccionEstandarNLectura cannot be null!!");
                                    }
                                    elementList.add(localDireccionEstandarNLectura);
                                }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static WSRespuestaDERQType parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            WSRespuestaDERQType object =
                new WSRespuestaDERQType();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"WSRespuestaDE-RQ-Type".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (WSRespuestaDERQType)co.net.une.www.svc.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                 
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","codigoSolicitud").equals(reader.getName())){
                                
                                                object.setCodigoSolicitud(co.net.une.www.gis.BoundedString15.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","codigoSistema").equals(reader.getName())){
                                
                                                object.setCodigoSistema(co.net.une.www.gis.BoundedString30.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","estadoExcepcion").equals(reader.getName())){
                                
                                                object.setEstadoExcepcion(co.net.une.www.gis.BoundedString30.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","estadoPagina").equals(reader.getName())){
                                
                                                object.setEstadoPagina(co.net.une.www.gis.BoundedString5.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","instalacion").equals(reader.getName())){
                                
                                                object.setInstalacion(co.net.une.www.gis.BoundedString18.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","codigoDireccion").equals(reader.getName())){
                                
                                                object.setCodigoDireccion(co.net.une.www.gis.BoundedString100.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","direccionNormalizada").equals(reader.getName())){
                                
                                                object.setDireccionNormalizada(co.net.une.www.gis.BoundedString250.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","direccionAnterior").equals(reader.getName())){
                                
                                                object.setDireccionAnterior(co.net.une.www.gis.BoundedString250.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","placa").equals(reader.getName())){
                                
                                                object.setPlaca(co.net.une.www.gis.BoundedString10.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","agregado").equals(reader.getName())){
                                
                                                object.setAgregado(co.net.une.www.gis.BoundedString100.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","remanente").equals(reader.getName())){
                                
                                                object.setRemanente(co.net.une.www.gis.BoundedString100.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","latitud").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLatitud(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","longitud").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLongitud(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","coordenadaX").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCoordenadaX(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","coordenadaY").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCoordenadaY(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","estrato").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEstrato(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToInteger(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","rural").equals(reader.getName())){
                                
                                                object.setRural(co.net.une.www.gis.BoundedString2.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","estadoGeoreferenciacion").equals(reader.getName())){
                                
                                                object.setEstadoGeoreferenciacion(co.net.une.www.gis.BoundedString1.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","codigoDireccionProveevor").equals(reader.getName())){
                                
                                                object.setCodigoDireccionProveevor(co.net.une.www.gis.BoundedString100.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","codigoPais").equals(reader.getName())){
                                
                                                object.setCodigoPais(co.net.une.www.gis.BoundedString2.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","codigoDepartamento").equals(reader.getName())){
                                
                                                object.setCodigoDepartamento(co.net.une.www.gis.BoundedString2.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","codigoMunicipio").equals(reader.getName())){
                                
                                                object.setCodigoMunicipio(co.net.une.www.gis.BoundedString8.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","codigoComuna").equals(reader.getName())){
                                
                                                object.setCodigoComuna(co.net.une.www.gis.BoundedString10.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","codigoBarrio").equals(reader.getName())){
                                
                                                object.setCodigoBarrio(co.net.une.www.gis.BoundedString10.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","codigoLocalizacionTipo1").equals(reader.getName())){
                                
                                                object.setCodigoLocalizacionTipo1(co.net.une.www.gis.BoundedString10.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","codigoDaneManzana").equals(reader.getName())){
                                
                                                object.setCodigoDaneManzana(co.net.une.www.gis.BoundedString14.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","codigoPredio").equals(reader.getName())){
                                
                                                object.setCodigoPredio(co.net.une.www.gis.BoundedString17.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","tipoAgregacionNivel1").equals(reader.getName())){
                                
                                                object.setTipoAgregacionNivel1(co.net.une.www.gis.BoundedString20.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","nombreLocalizacionTipo1").equals(reader.getName())){
                                
                                                object.setNombreLocalizacionTipo1(co.net.une.www.gis.BoundedString100.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","nombreBarrio").equals(reader.getName())){
                                
                                                object.setNombreBarrio(co.net.une.www.gis.BoundedString100.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","nombreComuna").equals(reader.getName())){
                                
                                                object.setNombreComuna(co.net.une.www.gis.BoundedString100.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","direccionEstandarNLectura").equals(reader.getName())){
                                
                                                object.setDireccionEstandarNLectura(co.net.une.www.gis.BoundedString250.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
          